package com.zongtui.fourinone.coolhash;

import com.zongtui.fourinone.file.dump.DumpAdapter;
import com.zongtui.fourinone.file.FileAdapter.ByteReadParser;
import com.zongtui.fourinone.file.FileAdapter.ByteWriteParser;

public interface CoolHashBase{
	CoolHashException chex = new CoolHashException();
	ByteWriteParser bwp = DumpAdapter.getByteWriteParser();
	ByteReadParser brp = DumpAdapter.getByteReadParser();
	ConstantBit.Target ct = ConstantBit.Target.POINT;
	DumpAdapter dumpAdapter = new DumpAdapter("");
}