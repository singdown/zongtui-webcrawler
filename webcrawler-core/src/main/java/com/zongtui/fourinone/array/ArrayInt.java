package com.zongtui.fourinone.array;

import java.util.Arrays;

final class ArrayInt extends ArrayAdapter implements ArrayAdapter.ListInt{
	private int arraySize = 0x20000;
	private int[] arrayInt;
	private int arrayIndex = 0;
	
	public ArrayInt(){
		super();
		arrayInt = new int[arraySize];
		objArray[objIndex++]= arrayInt;
	}
	
	public void add(int[] initArr){
		for(int i: initArr)
			add(i);
	}
	
	public void add(int i){
		if(arrayIndex == arrayInt.length){
			arrayInt = new int[arraySize];
			auto();
			objArray[objIndex++]= arrayInt;
			arrayIndex =0;
		}
		arrayInt[arrayIndex++]=i;
	}
	
	public int size(){
		return (objIndex -1)* arraySize + arrayIndex;
	}
	
	public void set(int index, int i){
		((int[]) objArray[index/ arraySize])[index% arraySize]=i;
	}
	
	public int get(int index){
		return ((int[]) objArray[index/ arraySize])[index% arraySize];
	}
	
	public void sort(){
		IntSort is = new IntSort();
		is.arraySort(0, size() - 1);
	}
	
	public int[] sort(int[] arr){
		IntSort is = new IntSort(arr);
		is.intSort(0, arr.length - 1);
		return arr;
	}
	
	public int[] toArray(){
		int s = size();
		int[] m = new int[s];
		for(int i=0;i<s;i++)
			m[i]=get(i);
		return m;
	}
	
	public void order(int[] arr){
		Arrays.sort(arr);
	}
	
	private class IntSort{
		private int[] arr;
		IntSort(){}
		IntSort(int[] arr){
			this.arr = arr;
		}
		
		private void intSort(int k, int m)
		{ 
			int j=m,i=k; 
			for(;i<j;i++){ 
				int vai = arr[i];
				while(vai<=arr[i+1]){ 
					if(j==i+1)break; 
					int vai1 = arr[i+1]; 
					arr[i+1]=arr[j];
					arr[j--]=vai1; 
				} 
				
				if(vai>arr[i+1]){ 
					arr[i]=arr[i+1]; 
					arr[i+1]=vai; 
				} 
			} 
			if(i-1>k) 
				intSort(k, i - 1);
			if(m>i) 
				intSort(i, m);
		}
		
		private void arraySort(int k, int m)
		{ 
			int j=m,i=k; 
			for(;i<j;i++){ 
				int vai = get(i);
				while(vai<=get(i+1)){ 
					if(j==i+1)break; 
					int vai1 = get(i+1);
					set(i+1, get(j));
					set(j--, vai1);
				} 
				
				if(vai>get(i+1)){ 
					set(i, get(i+1));
					set(i+1, vai);
				} 
			} 
			if(i-1>k) 
				arraySort(k, i - 1);
			if(m>i) 
				arraySort(i, m);
		}
	}
}