<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>echarts demo管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>

</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/echartsdemo/echartsdemo/line5">echarts demo列表</a></li>
	</ul>
	<form:form id="searchForm" modelAttribute="echartsdemo" action="${ctx}/echartsdemo/echartsdemo/line5" method="post" class="breadcrumb form-search">
		<ul class="ul-form">
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	
	<div id="main" style="height:400px"></div>
	<script src="${ctxStatic}/echarts/echarts.js"></script>
<script type="text/javascript">
        // 路径配置
        require.config({
            paths: {
                echarts: '${ctxStatic}/echarts'
            }
        });
        
        // 使用
        require(
            [
                'echarts',
                'echarts/chart/line', // 使用线状图就加载line模块，按需加载
                'echarts/chart/bar' // 使用柱状图就加载bar模块，按需加载
            ],
            function (ec) {
                // 基于准备好的dom，初始化echarts图表
                var myChart = ec.init(document.getElementById('main')); 
                
            var option = {
            	    legend: {
            	        data:['高度(km)与气温(°C)变化关系']
            	    },
            	    toolbox: {
            	        show : true,
            	        feature : {
            	            mark : {show: true},
            	            dataView : {show: true, readOnly: false},
            	            magicType : {show: true, type: ['line', 'bar']},
            	            restore : {show: true},
            	            saveAsImage : {show: true}
            	        }
            	    },
            	    calculable : true,
            	    tooltip : {
            	        trigger: 'axis',
            	        formatter: "Temperature : <br/>{b}km : {c}°C"
            	    },
            	    xAxis : [
            	        {
            	            type : 'value',
            	            axisLabel : {
            	                formatter: '{value} °C'
            	            }
            	        }
            	    ],
            	    yAxis : [
            	        {
            	            type : 'category',
            	            axisLine : {onZero: false},
            	            axisLabel : {
            	                formatter: '{value} km'
            	            },
            	            boundaryGap : false,
            	            data : ['0', '10', '20', '30', '40', '50', '60', '70', '80']
            	        }
            	    ],
            	    series : [
            	        {
            	            name:'高度(km)与气温(°C)变化关系',
            	            type:'line',
            	            smooth:true,
            	            itemStyle: {
            	                normal: {
            	                    lineStyle: {
            	                        shadowColor : 'rgba(0,0,0,0.4)'
            	                    }
            	                }
            	            },
            	            data:[15, -50, -56.5, -46.5, -22.1, -2.5, -27.7, -55.7, -76.5]
            	        }
            	    ]
            	};
        
                // 为echarts对象加载数据 
                myChart.setOption(option); 
            }
        );
    </script>
	
</body>
</html>